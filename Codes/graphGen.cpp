// graphGen.cpp : Defines the entry point for the console application.
//


#define VERTEX_COUNT_LIMIT 99999
#define APPEND "a"
#define WRITE "w"
#define XML 1
#define TEXT 2

#include <cstring>
#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>
//#include <windows.h>

#include "Header.h" //string, vector, fstream


using namespace std;

	vector<string>labels;
	int graphCount, maxDegree;
	double avgVertextCount, vertexSigma;
	double avgEdgeWeight, weightSigma;
	double minW, maxW;
	int gid = 0;

char* filename = "graph_dataset.xml";
char* filename2 = "graph_dataset.txt";
int filetype = TEXT;


int getNormalValue(double mean, double stddev);
double rand_normal(double mean, double stddev);
vector<string> generateLabel(int labelCount);
string stringIncrement(string str);
int getRandom(int l, int u);
double getRandomWeight(double l, double u, Arc e);
Graph populateWithEdge(Graph g);
bool thereExists(Graph g,int u,int v);
bool writeGraph(Graph g, char* mode);
bool createDatasetFile(char* mode);
bool endDatasetFile(char* mode);



int main(){ 
	
	srand(time(NULL));
	int labelCount;
	int choice;
	
	/*for(int i = 0; i< 20; i++){
		cout<<getRandom(1, 20)<<endl;
	}*/

	cout<<"How many graphs? ";
	cin>>graphCount;
	cout<<"Average vertex count = ";
	cin>>avgVertextCount;
	cout<<"Vertex standard deviation = ";
	cin>>vertexSigma;
	cout<<"MaxDegree of a vertex = ";
	cin>>maxDegree;

	cout<<"minW = ";
	cin>>minW;
	cout<<"maxW = ";
	cin>>maxW;


	cout<<"many labels? ";
	cin>>labelCount;
	cout<<"For automatic label generation press 1\nFor manual label input press 2\n";
	
	cin>>choice;
	if(choice==1) generateLabel(labelCount);
	else for(int i =0; i<labelCount; i++){
		string temp;
		cin>>temp;
		labels.push_back(temp);
	}
	//cout<<labels.size()<<endl;
	string ch;
	cout<<"Press 1 to get output as xml\nPress any other key to get output as text file"<<endl;
	cin>>ch;
	if(ch.at(0)=='1') filetype =XML;

	int minVertexCount = 9999999999, maxVertexCount = -1;
	//vector<Graph>gdb;
	createDatasetFile(WRITE);
	for(int i = 1; i<=graphCount; i++){
		//cout<<i<<":"<<getNormalValue(avgVertextCount, vertexSigma)<<endl;
		int vc = getNormalValue(avgVertextCount, vertexSigma);
		if(vc>maxVertexCount) maxVertexCount = vc;
		if(vc<minVertexCount) minVertexCount = vc;
		int lmaxDegree = vc-1;
		if(lmaxDegree > maxDegree ) lmaxDegree = maxDegree;
		int maxEdgeCount = floor((vc*lmaxDegree)/2);
		double mean = (maxEdgeCount - vc + 1)/1.50;
		double sd = mean/4;
		int ec = (vc-1) + getNormalValue(mean,sd);
		while(ec>maxEdgeCount || ec < (vc-1)){
			ec = (vc-1) + getNormalValue(mean,sd);
		}
		cout<<i<<" : #vertex = "<<vc<<" \& #edge = "<<ec<<endl;
		Graph g = Graph();
		g.id = i;
		g.ec = ec;
		g.vc = vc;
		g = populateWithEdge(g);
		if(!writeGraph(g,APPEND)){
			cout<<"ERROR: Cannont write in "<<filename<<endl;
			break;
		}

		
	}
	endDatasetFile(APPEND);
	/*for(int i = 0; i<gdb.size(); i++){
		cout<<i<<" : #vertex = "<<gdb[i].vc<<" \& #edge = "<<gdb[i].ec<<endl;
	}
	cout<<"Minimum vertex count = "<<minVertexCount <<" and maximum vertex count = "<<maxVertexCount<<endl;*/


	return 0;
}


int getNormalValue(double meu, double sigma){
	double ret = rand_normal(meu,sigma);
	double ad = ret-floor(ret);
	if(ad<0.5) return floor(ret);
	return ceil(ret);
}



double rand_normal(double mean, double stddev)
{//Box muller method
    static double n2 = 0.0;
    static int n2_cached = 0;
//	double ret;
    if (!n2_cached)
    {
        double x, y, r;
        do
        {
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);
        {
            double d = sqrt(-2.0*log(r)/r);
            double n1 = x*d;
            n2 = y*d;
            double result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else
    {
        n2_cached = 0;
        return n2*stddev + mean;
    }
}


vector<string> generateLabel(int labelCount){
	//vector<string>labels;
	if(!labelCount) return labels;
	else labels.push_back("a");
	int pindex = 0;
	for(int i = 1; i< labelCount; i++){
		if(labels[i-1].at(labels[i-1].length()-1) < 'z') {
			labels.push_back(labels[i-1]);
			labels[i].replace(labels[i].length()-1, 1, 1, labels[i].at(labels[i].length()-1)+1);
			//cout<<labels[i]<<endl;
		}else{
			string str = labels[pindex++];
			//cout<<"pindex: "<<str<<endl;
			str += "a";
			labels.push_back(str);
		}
	}

	cout<<"generated labes: "<<endl;
	for(unsigned int i = 0; i<labels.size(); i++){
		cout << labels[i] << " ";
	}
	cout<<endl;

	return labels;
}

int getRandom(int l, int u){
	int k = u-l;
    return rand() % k + l;
}

vector<Arc>uniqueEdge;

double getRandomWeight(double l, double u, Arc e){

	for(unsigned int i = 0; i <uniqueEdge.size(); i++){
		if((uniqueEdge[i].u.nlabel == e.u.nlabel && uniqueEdge[i].v.nlabel == e.v.nlabel) || (uniqueEdge[i].u.nlabel == e.v.nlabel && uniqueEdge[i].v.nlabel == e.u.nlabel)){
			return uniqueEdge[i].weight;
		}
	}

	int low = l*100, up = u * 100; //precision upto two decimal place
	double k = getRandom(low, up);
	k = k/100;
	e.weight = k;
	uniqueEdge.push_back(e);
	return k;
}

Graph populateWithEdge(Graph g){
	//generate spanning tree
	//g.vc-1 edges here
	int grp[VERTEX_COUNT_LIMIT];
	int usedDeg[VERTEX_COUNT_LIMIT];
	for(int i = 0; i<=g.vc; i++) {
		grp[i] = - 1;
		usedDeg[i] = 0;
	}
	for(int i=0; i<g.vc-1; i++){
		int u = getRandom(0,g.vc);
		if(usedDeg[u]>=maxDegree){
			i--;
			continue;
		}
		int v = getRandom(0,g.vc);
		//cout<<"u = "<<u<<endl;
		//cout<<"v = "<<v<<endl;
		while(u==v || usedDeg[v]>=maxDegree){
			v = getRandom(0,g.vc);
			//cout<<"v = "<<v<<endl;
		}
		if(grp[u]==-1 && grp[v]==-1){
			
			grp[u] = u;
			grp[v] = u;
			Node un,vn;
			un.nid = u;
			vn.nid = v;
			un.nlabel = labels[getRandom(0,labels.size())];
			vn.nlabel = labels[getRandom(0,labels.size())];
			//cout<<un.nlabel << vn.nlabel <<endl;
			g.vertex.push_back(un);
			g.vertex.push_back(vn);
			Arc e;
			e.u = un;
			e.v = vn;
			e.weight = getRandomWeight(minW, maxW, e);
			g.edge.push_back(e);
			usedDeg[u]++;
			usedDeg[v]++;
			//cout<<"reached1";
		}else if(grp[u]!=grp[v]){
			if(grp[u]==-1) {
				//cout<<"aka 1"<<endl;
				grp[u] = grp[v];
				Node un, vn;
				un.nid = u;
				un.nlabel = labels[getRandom(0,labels.size())];
				cout<<un.nlabel<<endl;
				g.vertex.push_back(un);
				vn = g.getNode(v);
				//cout<<"Found"<<vn.nid<<endl;
				Arc e;
				e.u = un;
				e.v = vn;
				e.weight = getRandomWeight(minW, maxW, e);
				g.edge.push_back(e);
				usedDeg[u]++;
				usedDeg[v]++;
			}else if(grp[v]==-1){
				//cout<<"aka 2"<<endl;
				grp[v] = grp[u];
				Node un, vn;
				vn.nid = v;
				vn.nlabel = labels[getRandom(0,labels.size())];
				//cout<<vn.nlabel <<endl;
				g.vertex.push_back(vn);
				un = g.getNode(u);
				//cout<<"Found"<<un.nid<<endl;
				Arc e;
				e.u = un;
				e.v = vn;
				e.weight = getRandomWeight(minW,maxW, e);
				g.edge.push_back(e);
				usedDeg[u]++;
				usedDeg[v]++;
			}else{
				//cout<<"aka 3"<<endl;
				int k = grp[v];
				grp[v] = grp[u];
				for(int j = 0; j <g.vc ;j++){
					if(grp[j] == k) grp[j] = grp[u];
				}


				Node un, vn;
				//cout<<"aka 3.."<<u<<endl;
				un = g.getNode(u);
				//cout<<"Found"<<un.nid<<endl;
				//cout<<"aka 3.."<<v<<endl;
				vn = g.getNode(v);
				//cout<<"Found"<<vn.nid<<endl;
				Arc e;
				e.u = un;
				e.v = vn;
				e.weight = getRandomWeight(minW,maxW, e);
				g.edge.push_back(e);
				usedDeg[u]++;
				usedDeg[v]++;
			}
			
		}else i--;
	}

	//add g.ec-g.vc+1 edges here
	for(int i = g.vc-1; i<g.ec; i++){
		int u = getRandom(0,g.vc);
		if(usedDeg[u]>=maxDegree){
			i--;
			continue;
		}

		int v = getRandom(0,g.vc);
		//cout<<"u = "<<u<<endl;
		//cout<<"v = "<<v<<endl;
		while(u==v || usedDeg[v]>=maxDegree){
			v = getRandom(0,g.vc);
			//cout<<"v = "<<v<<endl;
		}

		if(thereExists(g,u,v)){
			i--;
			continue;
		}
		Node un, vn;
		un = g.getNode(u);
		vn = g.getNode(v);
		Arc e;
		e.u = un;
		e.v = vn;
		e.weight = getRandomWeight(minW,maxW, e);
		g.edge.push_back(e);
	}

	//print
	g.print();
	return g;
}

bool thereExists(Graph g,int u,int v){
	for(unsigned int i = 0; i< g.edge.size(); i++){
		if((u==g.edge[i].u.nid && v==g.edge[i].v.nid) || (v==g.edge[i].u.nid && u==g.edge[i].v.nid)) return true;
	}
	return false;
}

bool first = true;
FILE *fpg, *fpx;
bool writeGraph(Graph g, char* mode){
	//cout<<"Implement writeGraph function"<<endl;
	FILE *fp;
	errno_t err;
	if(filetype==TEXT){
		if(first){
			err	= fopen_s(&fp, filename2,mode);
			if(err!=0) return false;
			fpg = fp;
			first = false;
		}
		
		//fprintf_s(fp,"%d %d %d ", gid++, g.vertex.size(), g.edge.size());
		
		for(unsigned int i = 0; i<g.vertex.size(); i++){
			fprintf_s(fpg,"%d %s ", g.vertex[i].nid, g.vertex[i].nlabel.c_str());
		}
		fprintf_s(fpg,": ");
		for(unsigned int i = 0; i<g.edge.size(); i++){
			fprintf_s(fpg,"%d %d %.2lf ", g.edge[i].u.nid, g.edge[i].v.nid, g.edge[i].weight);
		}
		fprintf(fpg,"\n");
		//fclose(fp);
		return true;
	}
	if(first){
		first = false;
		err	= fopen_s(&fp, filename,mode);
		if(err!=0) return false;
		fpx = fp;
	}
	fprintf_s(fpx,"	<graph>\n");
	fprintf_s(fpx,"		<vertices>\n");
	for(unsigned int i = 0; i<g.vertex.size(); i++){
		fprintf_s(fpx,"			<node>\n");
		fprintf_s(fpx,"				<nid>%d</nid>\n",g.vertex[i].nid);
		fprintf_s(fpx,"				<nlabel>%s</nlabel>\n",g.vertex[i].nlabel.c_str());
		fprintf_s(fpx,"			</node>\n");
	}
	fprintf_s(fpx,"		</vertices>\n");
	fprintf_s(fpx,"		<arcs>\n");
	for(unsigned int i = 0; i<g.edge.size(); i++){
		fprintf_s(fpx,"			<edge>\n");
		fprintf_s(fpx,"				<u>%d</u>\n",g.edge[i].u.nid);
		fprintf_s(fpx,"				<v>%d</v>\n",g.edge[i].v.nid);
		fprintf_s(fpx,"				<weight>%.2lf</weight>\n",g.edge[i].weight);
		fprintf_s(fpx,"			</edge>\n");
	}
	fprintf_s(fpx,"		</arcs>\n");
	fprintf_s(fpx,"	</graph>\n");
	return true;
}

bool createDatasetFile(char* mode){
	
	FILE *fp;
	errno_t err;
	if(filetype==TEXT){
		err	= fopen_s(&fp, filename2,mode);
		if(err!=0) return false;
		fclose(fp);
		return true;
	}
	err	= fopen_s(&fp, filename,mode);
	if(err!=0) return false;
	fprintf_s(fp,"<graphDataset>\n");
	fclose(fp);
	return true;
}
bool endDatasetFile(char* mode){
	if(filetype==TEXT) {
		fclose(fpg);
		return true;
	}
	//FILE *fp;
	//errno_t err;
	//err	= fopen_s(&fp, filename,mode);
	//if(err!=0) return false;
	fprintf_s(fpx,"</graphDataset>");
	fclose(fpx);
	return true;
}
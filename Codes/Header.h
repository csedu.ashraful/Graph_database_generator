#define MAXLENGTH 999999

#include <vector>
#include <string>
#include <fstream>

using namespace std;

struct Node{
	int nid;
	string nlabel;
};

struct Arc{
	Node u, v;
	double weight;
};

class Graph
{
public:
	Graph();
	~Graph();
	void print();
	int vc,ec,id;
	vector<Node>vertex;
	vector<Arc>edge;
	Node getNode(int nid);

	
private:

};

Graph::Graph()
{
}

Graph::~Graph()
{
}

void  Graph::print(){
		cout<<id<<" : "<<"("<<vc<<","<<ec<<")"<<endl;
		for(unsigned int i=0; i<edge.size(); i++){
			cout<<"        <"<<edge[i].u.nid<<","<<edge[i].v.nid<<","<<edge[i].u.nlabel<<","<<edge[i].v.nlabel<<","<<edge[i].weight<<">"<<endl;
		}
}

Node Graph::getNode(int nid){
		//cout<<"aka ..getNode"<<nid<<endl;
		for(unsigned int i = 0; i<vertex.size(); i++){
			if(vertex[i].nid == nid) return vertex[i];
		}
		//cout<<"nid "<<nid<< "not found"<<endl;
			print();
			return Node();
	}

struct gfp_pair{
	Graph g;
	FILE *fp;
};

gfp_pair readNextGraph(FILE *fp){
	
	gfp_pair temp;
	char* str = NULL;
	if( fgets (str, MAXLENGTH, fp)!=NULL ) 
	{
      puts(str);
	  temp.fp = fp;

	}else{
		temp.fp = fp;
		temp.g.vc = 0;
		temp.g.ec = 0;
		return temp;
	}

}
